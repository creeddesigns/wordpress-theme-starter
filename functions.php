<?php
//update_option('siteurl','http://local.authoring.accusoft.com');
//update_option('home','http://local.authoring.accusoft.com');

//add_filter('xmlrpc_enabled', '__return_false');

if ( ! isset( $content_width ) )
	$content_width = 750; /* pixels */

function return_echo($func) {
    ob_start();
    $func;
    return ob_get_clean();
}

function seoUrl($string) {
    $string = strtolower($string);
	$string = preg_replace('/[^a-zA-Z0-9\']/', '_', $string);
	$string = str_replace(array('"', "'"), '_', $string);
    return $string;
}

function page_title_trail($title) {
	$title = empty($title) ? get_the_title() : $title;
	$title = strtolower($title);
	if(is_archive()):
		$sub_title = '/'.get_queried_object()->labels->name;
	elseif($title === get_the_title()):
		$sub_title = null;
	else:
		$sub_title = '/'.get_the_title();
	endif;
	$sub_title = strtolower($sub_title);
	echo'<h4 class="h4 accent-off-white ow">'.$title.'<span class="subhead-title"> '.$sub_title.'</span></h4>';
}

// Grabbing current page template
add_filter( 'template_include', 'var_template_include', 1000 );
function var_template_include( $t ){
  $GLOBALS['current_theme_template'] = basename($t);
  return $t;
}
function get_current_template( $echo = false ) {
	if( !isset( $GLOBALS['current_theme_template'] ) ) return false;
	if( $echo ) echo $GLOBALS['current_theme_template'];
	else  return $GLOBALS['current_theme_template'];
}

// Function to get ID from page name and parent match.
function get_ID_by_page_name($page_name,$parentid){
	     global $wpdb;
	     $page_name_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name ='".$page_name."' AND post_parent ='".$parentid."'");
	     return $page_name_id;
}

function get_functions(){
	foreach ( glob( dirname(__FILE__) . "/includes/functions/*.php") as $filename):
	    require_once($filename);
	endforeach;
}get_functions();

function list_pages_filter($query){
	if(is_admin() && ($query['post_type'] == 'products')){
		$query['depth'] = 1;
	}
	return $query;
}
add_filter( 'page_attributes_dropdown_pages_args', 'list_pages_filter' );

add_action( 'init', 'create_product_cat' );
function create_product_cat() {
	register_taxonomy(
		'product_category',
		'products',
		array(
			'label' => __( 'Product Category' ),
			'rewrite' => array( 'slug' => 'form' ),
			'hierarchical' => true,
		)
	);
	register_taxonomy_for_object_type( 'product_category', 'products');
}

add_action( 'init', 'create_product_func' );
function create_product_func() {
	register_taxonomy(
		'product_function',
		'products',
		array(
			'label' => __( 'Product Function' ),
			'rewrite' => array( 'slug' => 'function' ),
			'hierarchical' => true,
		)
	);
	register_taxonomy_for_object_type( 'product_function', 'products');
}

add_action( 'init', 'create_product_type' );
function create_product_type() {
	register_taxonomy(
		'product_type',
		'products',
		array(
			'label' => __( 'Product Type' ),
			'rewrite' => array( 'slug' => 'type' ),
			'hierarchical' => true,
		)
	);
	register_taxonomy_for_object_type( 'product_type', 'products');
}

function new_excerpt_more( $more ) {
	//return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'your-text-domain') . '</a>';
	return ' ...';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );


add_filter('new_royalslider_skins', 'new_royalslider_add_custom_skin', 10, 2);
function custom_excerpt_length( $length ) {
	 return 15;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// Global Variables
define('home', get_home_url() );
